<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 2</title>  
</head>
<body>
    <?php
    // Concatenación de Cadenas
    $titulo = "Concatenación de Cadenas";

    // Definir dos variables con cadenas
    $cadena1 = "Hola, ";
    $cadena2 = "esta es mi primera concatenación!";

    // Concatenar las cadenas usando el operador punto (.)
    $resultado = $cadena1 . $cadena2;

    // Imprimir el resultado en pantalla
    echo '<h1>'. $titulo .'</h1>';
    echo '<p>' . $resultado . '</p>';
    ?>
</body>
</html>
