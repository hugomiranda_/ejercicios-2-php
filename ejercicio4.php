<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 4</title>
    <style>
        table {
            width: 50%;
            border-collapse: collapse;
            margin: 20px auto;
        }

        th, td {
            border: 1px solid #000;
            padding: 8px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }

        tr:nth-child(odd) {
            background-color: #e0e0e0;
        }

        tr:nth-child(even) {
            background-color: #ffffff;
        }

        h1 {
            text-align: center;
        }
    </style>
</head>
<body>
    <h1>Tabla de Multiplicar del 9</h1>
    <table>
        <thead>
            <tr>
                <th>Multiplicando</th>
                <th>Resultado</th>
            </tr>
        </thead>
        <tbody>
            <?php
            // Iterar para generar la tabla de multiplicar del 9
            for ($i = 1; $i <= 10; $i++) {
                $resultado = 9 * $i;

                // Determinar la clase de fila (par o impar) para alternar los colores
                $clase_fila = ($i % 2 == 0) ? 'class="fila-par"' : '';

                // Imprimir fila con el multiplicando y el resultado
                echo "<tr $clase_fila><td>9 x $i</td><td>$resultado</td></tr>";
            }
            ?>
        </tbody>
    </table>
</body>
</html>
