<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 3</title>
</head>
<body>
    <?php
    $titulo = "Tipos de Datos en PHP";
    echo '<h1>'. $titulo .'</h1>';

    // Crear una variable con el contenido 24.5
    $variable = 24.5;

    // Determinar el tipo de datos de la variable y mostrarlo en pantalla con un carácter de nueva línea
    echo 'Crear una variable con el siguiente contenido 24.5: ' . gettype($variable) . '<br>';

    // Modificar el contenido de la variable a "HOLA"
    $variable = "HOLA";

    // Determinar el nuevo tipo de datos de la variable y mostrarlo en pantalla con un carácter de nueva línea
    echo 'Modificar el contenido de la variable al valor “HOLA”: ' . gettype($variable) . '<br>';

    // Cambiar el tipo de la variable a entero
    settype($variable, "integer");

    // Usar var_dump para mostrar el contenido y tipo de la variable
    echo 'Setear el tipo de la variable que contiene “HOLA” a un tipo entero: '; 
    var_dump($variable);
    ?>
</body>
</html>
