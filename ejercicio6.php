<?php
$parcial1 = rand(0, 30);
$parcial2 = rand(0, 20);
$final1 = rand(0, 50);

$nota_final = $parcial1 + $parcial2 + $final1;

switch (true) {
    case $nota_final >= 90:
        $nota = "5";
        break;
    case $nota_final >= 80 && $nota_final < 90:
        $nota = "4";
        break;
    case $nota_final >= 70 && $nota_final < 80:
        $nota = "3";
        break;
    case $nota_final >= 60 && $nota_final < 70:
        $nota = "2";
        break;
    default:
        $nota = "1";
        break;
}

echo "El puntaje final del alumno es $nota_final. Su nota es un: $nota";
?>
