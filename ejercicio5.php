<?php
// Se generan los valores aleatorios para las 3 variables
$a = rand(99, 999);
$b = rand(99, 999);
$c = rand(99, 999);

$expresionA = $a * 3;
$expresionB = $b + $c;

// Resultado basado en operador ternario
$resultado = ($expresionA > $expresionB) ? "La expresión A: $expresionA es mayor que la expresión B: $expresionB" : "La expresión B: $expresionB es mayor o igual que la expresión A: $expresionA";

echo $resultado;
?>
