<?php
$contador = 0;

// Recorrera hasta que se cuenten con 900 números
while ($contador < 900) {
    $numero = rand(1, 10000); 
    if ($numero % 2 === 0) {  // Si es par, se imprime el numero y se suma el contador
        echo $numero . " ";
        $contador++;
    }
}

echo "<br>Total de números impresos:" . $contador;
?>

