<!DOCTYPE html>
<html>
<head>
    <title>Ejercicio 1</title>
</head>
<body>
    <?php
    // Crear variable para el titulo
    $titulo = "Datos Personales";

    // Crear una variable que almacene su nombre y apellido
    $nombreApellido = "Lucas Ojeda";

    // Crear una variable que guarde su nacionalidad
    $nacionalidad = "Paraguayo";

    // Usar la función echo para imprimir en pantalla el contenido de la variable que almacena su nombre y apellido en negrita y rojo
    echo '<h1>'. $titulo . '</h1>';
    echo '<p><strong><span style="color: red;">' . $nombreApellido . '</span></strong></p>';

    // Usar la función print para imprimir en pantalla el contenido de la variable que almacena su nacionalidad subrayado
    print('<p><u>' . $nacionalidad . '</u></p>');
    ?>
</body>
</html>
